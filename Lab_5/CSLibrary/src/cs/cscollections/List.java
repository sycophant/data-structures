package cs.cscollections;

public interface List<E>{
	void append(E ele);
	void prepend(E ele);
	void insertAfter(E first, E second);
	void remove(E ele);
	boolean search(E ele);
	boolean isEmpty();
	int getLength();
}