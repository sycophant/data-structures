package cs.cscollections;

public class SinglyLinkedList<E> implements List<E>{
	private Node<E> head;
	private int size = 0;

	public SinglyLinkedList(){}
	public void append(E ele){
		if (head == null){
			prepend(ele);
		}else{
			Node<E> curNode = head;
			while(curNode.getNext() != null){
				curNode = curNode.getNext();
				size++;
			}
			curNode = new Node<E>(ele);
		}
	}
	public void prepend(E ele){
		Node<E> newNode = new Node<E>(ele);
		if(this.head != null){
			newNode.setNext(this.head);
		}
		this.head = newNode;
		size++;
	}
	public void insertAfter(E first, E second){}
	public void remove(E ele){}
	public boolean search(E ele){
		return false;
	}
	public boolean isEmpty(){
		return head == null;
	}
	public int getLength(){
		return size;
	}
}