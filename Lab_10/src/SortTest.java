import cs.cscollections.sorts.Sorts;

import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;

//quicksort test.`
public class SortTest{
	public static void main (String [] args){

		Sorts.setArray(getFile());

		Sorts.sort();

		System.out. printf("File with %d integers found.\n", Sorts.a.length);
		for(int i=1; i<=Sorts.a.length - 1; i++){
			if(i%3==0){
				System.out.printf("Number %d  is %d.\n", i, Sorts.a[i]);
			}
			else{
				System.out.printf("Number %d  is %d: ", i, Sorts.a[i]);
			}
		}
		int compCount = Sorts.getComp();
		System.out.println("\nNumber of comparisons: "+ compCount);
		Sorts.clearComp();
	}

	public static int[] getFile(){  //pulls file
        Scanner in = null;
        try{in = new Scanner(new File("randIntegersA.in"));}
        catch (FileNotFoundException e){
            System.out.println("No file.");
            System.exit(1);
        }
        int size = in.nextInt();
        int []array = new int[size];
        for(int i=0;i<size;i++){array[i] = in.nextInt();}
        return array;
    }
}
