import java.util.ArrayDeque;
/**
 * <p>The Maze class represents a maze. The following methods 
 * are provided: </p>
 *<ul>
 *  <li>boolean traverse() - determines if the maze can be
 *                           traversed.</li>
 *  <li>void toString()    - converts the maze to a string.
 * </ul>
 * 
 * It is assumed that the entry to the maze is at (0,0) and
 * the exit from the maze is in the lower right corner.
 * Adapted from: Lewis and Chase, Java Software Structures: 
 * Designing and Using Data Structures.
 * 
 * @author S. Sigman 
 * @version 1.0 3/26/2017
 */
public class Maze{
    private final int TRIED = 3;
    private final int PATH = 7;
    private int [][] grid = { {1,1,1,0,1,1,0,0,0,1,1,1,1},
                            {1,0,0,1,1,0,1,1,1,1,0,0,1},
                            {1,1,1,1,1,0,1,0,1,0,1,0,0},
                            {0,0,0,0,1,1,1,0,1,0,1,1,1},
                            {1,1,1,0,1,1,1,0,1,0,1,1,1},
                            {1,0,1,0,0,0,0,1,1,1,0,0,1},
                            {1,0,1,1,1,1,1,1,0,1,1,1,1},
                            {1,0,0,0,0,0,0,0,0,0,0,0,0},
                            {1,1,1,1,1,1,1,1,1,1,1,1,1} };
    int maxX = grid.length-1;
    int maxY = grid[0].length-1;
    Position end = new Position(maxX, maxY);
    /**
    * Attempts to iteratively traverse the maze.  It inserts special
    * characters indicating locations that have been tried and that
    * eventually become part of the solution.  This method uses a 
    * stack to keep track of the possible moves that could be made.
    * @return True if the maze can be traversed. Otherwise, false.
    */
    public boolean traverse (){
        boolean done = false;  //defaults to false.
        Position pos = new Position(); //Starts at top left.
        ArrayDeque<Position> stack = new ArrayDeque<>();
        stack.addFirst(pos);  // put the first position on the stack
        grid[0][0] = TRIED;   // mark the position T

        while (!done){
            pos = stack.peek();
            Position curPos = nextMove(pos);
            if((curPos==null)){
                pos = stack.pop();
                curPos = pos;
            }else{
                stack.push(curPos);
                grid[curPos.getX()][curPos.getY()] = TRIED;
                pos = curPos;}
            if((pos.getX() == end.getX() && pos.getY() == end.getY()) || stack.isEmpty()){
                done = true;}
        }
        if(done && (pos.getX() == end.getX() && pos.getY() == end.getY())){
            while((pos.getX()!=0 && pos.getY()!=0) && !stack.isEmpty()){
                Position latest = stack.peek();
                grid[latest.getX()][latest.getY()] = PATH;
                stack.pop();
            }
        }
        return done;
    }
    /**
    * isMoveValid determines whether a given move is valid.  Validity
    * is determined by 1) if the position is not a wall, 2) the position
    * has not been previously visited, and 3) the position is within the
    * limits of the maze.
    * @param curPos The Position to check.
    * @return True if the move is valid. False, otherwise.
    */

    private boolean isMoveValid (Position curPos){
        boolean valid = false;
        if((curPos.getX()<0 || curPos.getY()<0) || (curPos.getX()>maxX || curPos.getY()>maxY)){
            valid = false;}
        else if((grid[curPos.getX()][curPos.getY()]==3) || (grid[curPos.getX()][curPos.getY()]==0)){
            valid = false;}
        else if(curPos==end){valid = false;}
        else valid = true;
        return valid;
    }
    /*
    * nextMove returns the next valid move or null if no move
    * is possible.
    * @param curPos - The current position in the maze.
    * @return If there is a valid move, the position of that move
    *         is returned.  If there is no valid move, null is 
    *         returned.
    */
    private Position nextMove(Position curPos){
        Position posN = new Position(curPos.getX()-1, curPos.getY());
        Position posE = new Position(curPos.getX(), curPos.getY()+1);
        Position posW = new Position(curPos.getX(), curPos.getY()-1);
        Position posS = new Position(curPos.getX()+1, curPos.getY());

        if(isMoveValid(posE)){return posE;}
        else if(isMoveValid(posS)){return posS;}
        else if(isMoveValid(posW)){return posW;}
        else if(isMoveValid(posN)){return posN;}
        else return null;
    }
    /**
    * Returns a printable version of the maze as a string.  The maze is
    * encoded as follows:
    * <ul>
    *     <li>0 - represents a wall.</li>
    *     <li>1 - represents an available, non-visited, position.</li>
    *     <li>3 - represents a non-wall position that has been visited.</li>
    *     <li>7 - represents a position on te path through the maze.</li>
    * </ul>
    * @return The a printable version of the maze.
    */
    public String toString (){
        String result = "\n";
        //S is valid if:
        //1) The position is not a wall.
        //2) The position has not been previously visited.
        //3) The position is within the maze.tring result = "\n";
        for (int row=0; row < grid.length; row++){
            for (int column=0; column < grid[row].length; column++){
                result += grid[row][column] + "";}
        result += "\n";}
        return result;
    }
}