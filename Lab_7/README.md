# Lab7-AmazingMazesStarterCode

## Description
Starter Code for Lab 7-CSCI 261 Spring 2109.

## Directions
Follow the directions in the document Lab 7 - Amazing Mazes PDF document.  The document is available on the class Moodle page.
