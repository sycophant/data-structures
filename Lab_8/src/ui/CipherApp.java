package ui;

import cipherengine.CaesarCipherEngine;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * CipherApp implements the shifted Caesar Cipher algorithm.
 * It allows a string (the cleartext) to be encoded using
 * a sequence of integer keys, each of which specifies a shift.
 * It also allows an encrypted string (the ciphertext) to be
 * decoded given an appropriate set of keys.
 *
 * <blockquote><strong>Note:</strong> Only printable characters, those whose
 * ASCII code is between 32 and 127, inclusive, to be encoded or
 * decoded.  Characters outside this range are replaced with a
 * space character (character 32) prior to being encoded.</blockquote>
 *
 * 
 * @author S. Sigman
 * @version v. 1.0
 */
public class CipherApp {
    /**
     * in - A scanner object associated with the keyboard used to
     * capture user input.
     */
    private Scanner in;
    
    /**
     * Constructor to create an instance of a Cipher Application.
     */
    public CipherApp(){
        in = new Scanner(System.in);
    }
    
    /**
     * Run - This method is the heart of the CaesarCipher app.
     * It is responsible for organizing and executing the
     * application's logic, which includes creating the user interface.
     */
    public void run(){
        int [] keys = null; // Array of keys to use in the cipher
        
        // Print user directions
        printDirections();
        System.out.println();
        
        // Read in the keys
        keys = readKeys();
        
        // Make a CipherEngine to encode of decode the message
        CaesarCipherEngine cipher = new CaesarCipherEngine(keys);
        
        char cont = 'y';
        while (cont == 'y' || cont=='Y') {
            System.out.print("Encode or decode message? (e or d) => ");
            String prompt = in.nextLine();
            if(prompt.charAt(0) == 'e'|| prompt.charAt(0) =='E'){
                String message = new String();
                System.out.print("Pass in the plaintext => ");
                message = in.nextLine();
                System.out.println(cipher.encode(message));
            }else if(prompt.charAt(0) == 'd'|| prompt.charAt(0) =='D'){
                String message = new String();
                System.out.print("Pass in the ciphertext => ");
                message = in.nextLine();
                System.out.println(cipher.decode(message));
            }else System.out.println("Illegal entry!");
            
            System.out.print("Encode or decode another message? (y or n) => ");

            // Read response to the encode/decode query.  Read the first
            // character entered on the line.  If only a new line character is
            // entered, the length of the contQuery String will be 0.
            String contQuery = in.nextLine();
            if (contQuery.length() > 0)
              cont = contQuery.charAt(0);
            else
              cont = 'n';
        }
        
        // Print an exit message
        System.out.println();
        System.out.println("Encoding and decoding of Messages is finished. Thank");
        System.out.println("you for using our application.");
    }
    
    public static void main(String [] args) {
        CipherApp app = new CipherApp();
        app.run();
    }
    
    /**
     * printDirections - Method to print the directions on the screen
    **/
    private void printDirections(){
        System.out.println("====================================================");
        System.out.println("= The Caesar Cipher application allows the user to =");
        System.out.println("= encode or decode a message using a Caesar        =");
        System.out.println("= Cipher, which rotates encoding through a set of  =");
        System.out.println("= specified keys.                                  =");
        System.out.println("====================================================");
    }
    
    /**
     * readKeys - This utility method reads the keys from the user and 
     * populates the keys array.
     * 
     * @return The array of keys read from the user.
     */
    public int [] readKeys(){
        //Prompt the user to enter the keys.
        System.out.println("Please enter a sequence of keys to use for encoding ");
        System.out.println("and decoding. Enter the keys  on a single line.     "); 
        System.out.println("Values should be separated by a single space. Use   ");
        System.out.println("a value of 0 to end input.");
        System.out.println();
        System.out.print("Keys => ");
        
        // read the keys into a temporary holding array
        ArrayList<Integer> temp = new ArrayList(); // used to simplify the input
        int curKey = in.nextInt();
        int numKeys = 0;
        while (curKey != 0) {
            temp.add(curKey);
            curKey = in.nextInt();
            numKeys++;
        }
        
        // The newline after the 0 is left in the input buffer.  In order to prepare
        // for other input, this newline must be read from the buffer.
        in.nextLine();
        
        // make a new int array and fill it
        int[] tempKeyArry = new int [numKeys];
        Iterator<Integer> it = temp.iterator();
        int i = 0;
        while(it.hasNext()) {
            tempKeyArry[i] = it.next();
            i++;
        }
        
        // send the array back
        return tempKeyArry;
        
    }
}
