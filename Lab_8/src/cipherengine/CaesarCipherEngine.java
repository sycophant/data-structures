package cipherengine;
import java.util.ArrayDeque;

/**
 * The CaesarCipherEngine implements a shifted key version of the
 * Caesar Cipher.   Use of the class requires the user to supply a 
 * set of key values.  Messages are then encoded and decoded against
 * the set using the methods encode and decode.
 * 
 * Note: Only the <em>printable</em> characters in the ASCII character
 * code are encoded/decoded. (i.e. Characters with ASCII value from 32 
 * to 126).  If a <em>control</em> character is encountered in a string,
 * a space (character 32) is encoded in its place.  The net effect of
 * this policy is that <em>control</em> characters will be lost in an
 * encode - decode cycle.
 * 
 * @author S. Sigman
 * @version v. 1.0
 */
public class CaesarCipherEngine
{
    /**
     * An array of the keys used in the cipher.
     */
    private int [] keys;
    /**
     * The queue of keys to use for encoding and decoding.  keyQ is 
     * initialized from the array keys, which defines the order of the 
     * keys for the cipher are used.
     */
    private ArrayDeque <Integer> keyQ;
    /**
     * CaesarCipherEngine - This constructor is responsible for initializing
     * the array of keys the cipher will use.
     * 
     * @param keys An array of keys the cipher will use for both encoding
     * and decoding.
     */
    public CaesarCipherEngine(int [] keys) {
        this.keys = new int[keys.length];
        for(int i=0;i<keys.length;i++){
            this.keys[i] = keys[i];
        }
        keyQ = new ArrayDeque<Integer>();
    }
    /**
     * encode - This method returns the CipherText message that
     * results from a shifter Caesar Cipher encoding of the provided
     * string.  
     * 
     * @param clearText The message to encode.
     * @return The ciphertext for the encrypted cleartext string.
     */
    public String encode(String plainText) {
        initializeKeyQ();
        plainText = plainText.toLowerCase();
        String cipherText = new String();
        for(int i=0; i<plainText.length(); i++){
            int key = (int)keyQ.peekFirst();
            keyQ.addLast(keyQ.removeFirst());
            int encChar = (char)((int)plainText.charAt(i) + key);
            while(encChar>126){asciiWrap(encChar);}
            cipherText+=(char)encChar;
        }
        return cipherText;
    }
    
    /**
     * decode - This method returns the ClearText decoding of the 
     * specified CipherText.
     * 
     * @param cipherText The message to decode.
     * @return The cleartext version of the specified ciphertext.
     */
    public String decode(String cipherText) {
        initializeKeyQ();
        cipherText = cipherText.toLowerCase();
        String clearText = new String();
        for(int i=0; i<cipherText.length(); i++){
            int key = (int)keyQ.peekFirst();
            keyQ.addLast(keyQ.removeFirst());
            int decChar = ((int)cipherText.charAt(i) + key);
            while(decChar>126){asciiWrap(decChar);}
            clearText+=(char)decChar;
        }
        return clearText;
    }
    /**
     * initializeKeyQ - This method is a private utility method whose 
     * purpose is to insert the set of values in the keyQ in the correct
     * initial order.  It is called by both the encode and decode methods
     * before encoding or decoding a message.
     */
    private void initializeKeyQ(){
        keyQ.clear();
        for(int i=0; i< keys.length; i++){
            keyQ.add(keys[i]);
        }
    }
    /**
     * asciiWrap: Linear transformation for character if out of bounds.
     * @param int code of the character 
     * @return wrapped value.
     */
    private int asciiWrap(int chara){
        if(chara>126){chara = 32 + (chara%126);}
        return chara;
    }
}
