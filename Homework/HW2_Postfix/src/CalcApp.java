import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * CalcApp is a wrapper for calling the postfix conversion and
 * basic DMAS calculation as found in the other classes.
 * @author A. Hasnat
 * @version v. 1.0
 */

public class CalcApp {
    public static void main(String[] args) throws IOException{
        String expression;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Expression in infix notation. No spaces!: ");
        expression = br.readLine();

        IFPFConv pc = new IFPFConv(expression); 
        System.out.print("Converted: "); 
        pc.printExpression();

        PFCalc calc = new PFCalc(pc.getPostfixAsList());
        System.out.println();
        System.out.println("Output: " + calc.result());
    }
}