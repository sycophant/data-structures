import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * PostFixCalculator implements basic DMAS calculation using
 * a switch for operators and a queue for implementation.
 * @author A. Hasnat
 * @version v. 1.0
 */

public class PFCalc{
    private List<String> expression = new ArrayList<String>();
    private Deque<Double> stack = new ArrayDeque<Double>();

    public PFCalc(List<String> pf) {expression = pf;}
    public BigDecimal result(){
        for(int i = 0; i != expression.size(); ++i){
            if(Character.isDigit(expression.get(i).charAt(0))){
                stack.addLast(Double.parseDouble(expression.get(i)));
            }else{
                double tempResult = 0;
                double temp;
                switch(expression.get(i)){
                    case "+": temp = stack.removeLast();
                              tempResult = stack.removeLast() + temp;
                              break;

                    case "-": temp = stack.removeLast();
                              tempResult = stack.removeLast() - temp;
                              break;

                    case "*": temp = stack.removeLast();
                              tempResult = stack.removeLast() * temp;
                              break;

                    case "/": temp = stack.removeLast();
                              tempResult = stack.removeLast() / temp;
                              break;
                }
                stack.addLast(tempResult);
            }
        }return new BigDecimal(stack.removeLast()).setScale(3, BigDecimal.ROUND_HALF_UP);
    }
}