import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * IFPFConv converts infix to postfix and pushes it onto a queue.
 * @author A. Hasnat (with assistance for some of the funkier methods)
 * @version v. 1.0
 */

    public class IFPFConv {
    private String infix;
    private Deque<Character> stack = new ArrayDeque<Character>();
    private List<String> postfix = new ArrayList<String>();
    /**
     * IFPFConv constructor initializes class and sets up a converted
     * infix expression right off the bat.
     * @param expression from user input.
     */
    public IFPFConv(String expression){
        infix = expression;
        convertExpression();
    }
    /**
     * convertExpression converts user input via stringbuilder and
     * pushes it to queue or elsewhere using for loop. Called at 
     * initialization.
     */
    private void convertExpression()
    {
        StringBuilder temp = new StringBuilder();
        for(int i = 0; i != infix.length(); ++i){           
            if(Character.isDigit(infix.charAt(i))){
                temp.append(infix.charAt(i));
                while((i+1) != infix.length() && (Character.isDigit(infix.charAt(i+1)) || infix.charAt(i+1) == '.')){
                    temp.append(infix.charAt(++i));}
                postfix.add(temp.toString());
                temp.delete(0, temp.length());}
            else{inputToStack(infix.charAt(i));}
        }
        clearStack();
    }
    /**
     * inputToStack pushes to queue after stripping notation.
     * @param input from above method with operators stripped.
     */
    private void inputToStack(char input){
        if(stack.isEmpty() || input == '(')
            stack.addLast(input);
        else{if(input == ')'){
            while(!stack.getLast().equals('(')){
                postfix.add(stack.removeLast().toString());}
            stack.removeLast();
            }else{if(stack.getLast().equals('('))stack.addLast(input);
                else{while(!stack.isEmpty() && !stack.getLast().equals('(') && getPrecedence(input) <= getPrecedence(stack.getLast())){
                        postfix.add(stack.removeLast().toString());}
                    stack.addLast(input);
                }
            }
        }
    }
    /**
     * getPrecedence implements DMAS logic.
     * @param op to get operators.
     */
    private int getPrecedence(char op){
        if (op == '+' || op == '-'){return 1;}
        else if (op == '*' || op == '/'){return 2;}
        else if (op == '^'){return 3;}
        else return 0;
    }
    /**
     * Getters and Setters (and miscellanous methods)
     */
    private void clearStack(){while(!stack.isEmpty()){postfix.add(stack.removeLast().toString());}}
    public void printExpression(){for(String str : postfix){System.out.print(str + ' ');}}
    public List<String> getPostfixAsList(){return postfix;}
}