/**
 * @author (S. Sigman
 * @version 1/21/2018
 */

public class CrosswordApp {

    // To Do: add an appropriate JavaDoc method comment.  The
    // parameter args is a list of commandline values passed to
    // the method.
    public static void main(String [] args) {
        // create the crossword board
        Crossword puzzle = new Crossword(10, 20);

        // put some letters in at random
        puzzle.putAtRandomPos('c');
        puzzle.putAtRandomPos('b');
        puzzle.putAtRandomPos('d');
        puzzle.putAtRandomPos('z');

        char baseChar = 'a';
        Random rand = new Random();
        for (int i = 0; i < 10; i++){
            int offset = rand.nextInt(26);
            char anotherChar = (char)((int)baseChar + offset);
            puzzle.putAtRandomPos(anotherChar);
        }

        // display the completed puzzle
        puzzle.print();

    }
}
