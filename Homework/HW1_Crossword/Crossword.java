
import java.util.Random;   // A random number generator

/**
 * Example of a simple crossword puzzle board.
 *
 * @author (S. Sigman
 * @version 1/21/2018
 */

public class Crossword
{
   // declare the board
   private char [][] crossword;  // the board
   private int rows;   // number of rows in the board
   private int cols;   // number of columns in the board

   // declare a random number generator
   private Random rand; // random number generator

   /**
    * Constructor to make a board of the specified
    * size.
    * 
    * @para rows The number of rows in the board.
    * @para cols The number of columns in the board.
    */
   public Crossword(int rows, int cols)
   {
       this.rows = rows;  // set instance variable rows to
                          // the parameter rows
       this.cols = cols;  // set the instance variable cols 
                          // to the parameter cols

       // declare the crossword puzzle structure
       crossword = new char[rows][];
       
       // use a loop to create each row with cols columns.
       for (int r=0; r < rows; r++) {
           crossword[r] = new char[cols];
       }

       // fill the empty board with blanks
       this.initFill();

       // make a random number generator
       rand = new Random();
   }
   
   /**
    * Method to fill the crossword with blanks.
    */
   
   public void initFill(){
      for (int i=0; i < rows; i++){
        for (int j=0; j < cols; j++){
          System.out.println(" ");
        }
        
      }
   }

    /**
     * putAtRandomPos places the specified character at a random position
     * within the crossword puzzle board.
     *
     * @param newChar The character to place on the board.
     */

   public void putAtRandomPos(char newChar) {
        int row = rand.nextInt(rows);
        int col = rand.nextInt(cols);

        // To Do: add newChar at position (row, col) in
        // crossword
   }

    /**
     * The print method displays text drawing of the crossword puzzle
     * on the System.out stream.
     */

   public void print() {
       // write the top border
       for (int c = 0; c < cols; c++){
           System.out.print("__");
       }
       // write the last border character and advance to the next line
       System.out.println("_");

       // write the body of the board
       for( int r = 0; r < rows; r++) {
           for(int c = 0; c < cols; c++) {
               System.out.print("|" + crossword[r][c]);
           }
           System.out.println("|");
       }

       // write the bottom border
       for (int c = 0; c < cols; c++){
           System.out.print("--");
       }
       // write the last border character and advance to the next line
       System.out.println("-");
   }
}
