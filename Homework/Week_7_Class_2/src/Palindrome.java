import java.util.Scanner;
import java.util.ArrayDeque;
/**
 * Palindrome is a Java application that tests a string to
 * determine if the string is a Palindrome or not.  The 
 * program prompts the user for a string to test and 
 * reports the results.  Strings may be composed of
 * letters and digits.  Special characters and blanks are
 * automatically screened out of the input string.
 * 
 * @author S. Simgna
 * @version v1 3/26/2017
 */
public class Palindrome
{
    /**
     * Application that tests a string to determine if it is a 
     * palindrome.
     *
     * @param args List of command line arguments - not used
     */
    public static void main(String [] args)
    {
        // 0. Print program title.
            System.out.println("!**** Palindrome Tester ****!");
            
        // 1. Get the string to test and convert to lowercase
            Scanner in = new Scanner(System.in);
            System.out.print("Enter a string to test. ");
            String palCandidate = in.nextLine();
            palCandidate = palCandidate.toLowerCase();
        
        // 2. Scan the string placing characters on the stack
            ArrayDeque<Character> palStack = new ArrayDeque();
            
            for (int i =  0; i < palCandidate.length(); i++) 
            {
                if (Character.isLetterOrDigit(palCandidate.charAt(i)))
                    palStack.push(palCandidate.charAt(i));
            }
        
        // 3. Test the string one character at a time
            boolean matches = true;   // assume the string is a palindrome
            int i = 0;
            while(i < palCandidate.length() && matches)
            {
                if (Character.isLetterOrDigit(palCandidate.charAt(i)))
                {
                    char curChar = palStack.pop();
                    if (curChar != palCandidate.charAt(i))
                       matches = false;
                } 
                i++;
            }
        
        // 4. Report the results
            if (matches)
                System.out.println(palCandidate + " is a palindrome.");
            else
                System.out.println(palCandidate + " is a not palindrome.");
                
    }

}
