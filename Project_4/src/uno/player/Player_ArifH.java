package uno.player;
import uno.uno_game.*;
import uno.cards.*;
import java.util.Iterator;
import java.util.Random;
/**
 * Implementation of instantiated player for Uno game.
 * @author A. Hasnat
 * @version v1.0
 */
public class Player_ArifH extends Player{
    private Random rand = new Random();
    public Player_ArifH(Hand hand, Game_Engine dealer){super(hand, dealer);}    //constructor to instantiate Hand and Game_Engine classes.
    /**
     * Main class, takes arguments. In this case, creats a deck, shuffles it, iterates through a 8 card draw,
     * and then increments counters to print out the types of cards drawn and total count.
     * @param args
     */
    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.shuffle();
        Hand hand = new Hand();
        for(int i = 0; i < 7; i++){hand.addCard(deck.dealCard());}  //for loop for drawing cards
        System.out.printf("My hand is: \n");
        for(UNO_Card curCard : hand){System.out.printf("Card: %s \n", curCard);}
        int numFaceCards = 0;
        int numActionCards = 0;
        for(UNO_Card curCard : hand){   //counters
            if(curCard instanceof Action_Card){numActionCards++;}
            if(curCard instanceof Face_Card){numFaceCards++;}
        }
        System.out.printf("Action Cards: %d - Face Cards: %d \n", numActionCards, numFaceCards);
        System.out.println("Card Count: " + hand.numberOfCards());
    }
    public String whoAreYou(){return new String("A. Hasnat");}  //whoami
    /**
     * Method to keep track of score. Special cases get 50 points, everything else gets 20. Face cards return card number.
     * @return score
     */
    public int scoreOnCards() {
        int score = 0;
        for (UNO_Card curCard : hand) {
            if (curCard instanceof Face_Card) {score += ((Face_Card) curCard).getFaceNumber();}     //treatment of face cards
            else if (curCard instanceof Action_Card) {      //treatment of action cards
                if ((((Action_Card) curCard).getAction() != Action_Card.WILD_DRAW_4) 
                    && (((Action_Card) curCard).getAction() == Action_Card.WILD)){score += 50;}     //2 sentinels for special case
                else {score += 20;}
            }
        }
        return score;
    }
    /**
     * Method to determine whether to pick a card by running the cards in hand through sentinels via while loop.
     * Takes top card as instantiated object, to compare against cards in hand.
     * @param topCard
     * @return cardCall
     */
    public UNO_Card pickCard(UNO_Card topCard) {
        Iterator<UNO_Card> hand_iterator = this.hand.iterator();
        UNO_Card cardCall = null;
        while (hand_iterator.hasNext() && cardCall == null) {   //iterates through hand.
            UNO_Card curCard = hand_iterator.next();
            if (curCard.getColor() == UNO_Card.BLACK) {cardCall = curCard;}     //plays a card if black aka no risk
            else if (curCard.getColor() == topCard.getColor()) {cardCall = curCard;}    //plays a card if same color as top card in play.
            else if (topCard instanceof Face_Card && curCard instanceof Face_Card) {    //iterates thrugh face cards
                if (((Face_Card)curCard).getFaceNumber() != ((Face_Card)topCard).getFaceNumber()){continue;}
                cardCall = curCard;
            }
            else {
                if (!(topCard instanceof Action_Card) || !(curCard instanceof Action_Card)  //iterates thrugh action cards
                 || ((Action_Card)curCard).getAction() != ((Action_Card)topCard).getAction()){continue;}
                cardCall = curCard;
            }
        }
        return cardCall;
    }
    /**
     * Additional method to determine whether to pick a card by running the cards in hand through sentinels via for loop.
     * Takes colour of a card.
     * @param colour
     * @return cardCall
     */
    public UNO_Card pickCardColor(int colour) {
        Iterator<UNO_Card> hand_iterator= this.hand.iterator();
        UNO_Card cardCall;
        UNO_Card curCard;
        for (cardCall = null; hand_iterator.hasNext();){
            curCard = hand_iterator.next();
            if (curCard.getColor() == colour){  //plays card if same colour
                cardCall = curCard;
                break;
            }
        }
        return cardCall;
    }
    /**
     * Play method. Implements special case scenarios for various wildcards and logic for picking by card or color comparison.
     * Each call of play method changes the hand (except in certain cases) so returns the count.
     * @param card
     * @param useColourInstead
     * @return cardCount
     */
    public int play(UNO_Card card, boolean useColourInstead){
        UNO_Card cardCall = null;
        boolean fourDrawn = false;
        boolean twoDrawn = false;
        if (card instanceof Action_Card && ((Action_Card)card).getAction() == Action_Card.DRAW_2 && this.dealer.doIDraw()) {    //logic for draw 2.
            for (int i = 0; i < 2; ++i) {this.hand.addCard(this.dealer.drawCard());}
            twoDrawn = true;
        }
        if (!twoDrawn) {
            if (card instanceof Action_Card && card.getColor() == UNO_Card.BLACK){  //check for special cards
                if (((Action_Card)card).getAction() == Action_Card.WILD){   //instance where pick by color is used
                    int colourCall = this.dealer.getCurrent_Color();
                    cardCall = this.pickCardColor(colourCall);
                }
                else if (this.dealer.doIDraw()) {   //logic for draw 4
                    for (int i = 0; i < 4; ++i) {this.hand.addCard(this.dealer.drawCard());}
                    fourDrawn = true;
                }
                else {
                    cardCall = this.pickCard(card);     //does standard draw
                    if (cardCall == null) {     ////If draw fails, falls back to pick by color
                        int playColour = this.dealer.getCurrent_Color();
                        cardCall = this.pickCardColor(playColour);
                    }
                }
            }
            else {
                cardCall = this.pickCard(card);     //does standard draw
            }
            if (cardCall == null && !fourDrawn && !twoDrawn) {  //in case after 4 cards are drawn and still no card is picked
                UNO_Card curCard = this.dealer.drawCard();      //draws a card, adds to hand
                this.hand.addCard(curCard);     //do various checks via sentinel
                if (curCard.getColor() == UNO_Card.BLACK) {cardCall = curCard;}     //special card check via colour call
                else if (card.getColor() == UNO_Card.BLACK) {int colourCall2 = this.dealer.getCurrent_Color();//sets up new int for current colour
                    if (colourCall2 == curCard.getColor()) {cardCall = curCard;}        //plays if colors match
                }else if (curCard.getColor() == card.getColor()) {cardCall = curCard;}  //checks again
                else if (curCard instanceof Face_Card && card instanceof Face_Card) {   //checks for face card
                    if (((Face_Card)curCard).getFaceNumber() == ((Face_Card)card).getFaceNumber()) {cardCall = curCard;}}//checks for NUMBER of face card
                else if (curCard instanceof Action_Card && card instanceof Action_Card  //checks for action card
                 && ((Action_Card)curCard).getAction() == ((Action_Card)card).getAction()) {cardCall = curCard;}//checks for similar card
            }
        }
        if (cardCall != null) { //trashes the card
            this.hand.remove(cardCall);
            this.dealer.discardCard(cardCall);
            if (cardCall instanceof Action_Card && ((Action_Card)cardCall).getAction() == Action_Card.WILD) {   //special wildcard that randomly sets color
                this.dealer.setPlayColor(this.rand.nextInt(4));
            }
        }
        else {this.dealer.notifyPass((Player)this);}    //updates game engine that player is passing
        int cardCount = this.hand.numberOfCards();  //counter methods
        if (cardCount == 1) {this.dealer.notifyUNO((Player)this);}  //check for win lose states
        if (cardCount == 0) {this.dealer.notifyWon((Player)this);}
        return cardCount;
    }
}