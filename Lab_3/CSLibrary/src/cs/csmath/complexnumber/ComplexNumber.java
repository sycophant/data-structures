package cs.csmath.complexnumber;
import cs.csmath.exceptions.ComplexNumberDivisionByZero;

public class ComplexNumber{
	private double realPart;
	private double imagPart;

	public ComplexNumber(double realPart, double imagPart){ //constructor
		this.realPart = realPart;
		this.imagPart = imagPart;
	}

	public ComplexNumber(){
		this(0.0, 0.0);
	}

	public void add(ComplexNumber otherCN){
		this.realPart += otherCN.realPart;
		this.imagPart += otherCN.imagPart;
	}
	public void sub(ComplexNumber otherCN){
		this.realPart -= otherCN.realPart;
		this.imagPart -= otherCN.imagPart;
	}
	public void mult(ComplexNumber otherCN){
		double newrealPart = this.realPart * otherCN.realPart - this.imagPart * otherCN.imagPart;
		this.imagPart = this.imagPart * otherCN.realPart + this.realPart * otherCN.imagPart;
		this.realPart = newrealPart;
	}
	public void div(ComplexNumber otherCN){
		double divisor = otherCN.realPart * otherCN.realPart + otherCN.imagPart * otherCN.imagPart;
		if(divisor == 0.0){
			throw new ComplexNumberDivisionByZero();
		}
		double newrealPart = (this.realPart * otherCN.realPart - this.imagPart * otherCN.imagPart) / divisor;
		this.imagPart = (this.imagPart*otherCN.realPart - this.realPart*otherCN.imagPart) / divisor;
		this.realPart = newrealPart;
	}
	public ComplexNumber conj(){
		ComplexNumber a = new ComplexNumber();
		a.realPart = this.realPart;
		a.imagPart = -1 * this.imagPart;
		return a;
	}
	public double abs(){
		return Math.sqrt(this.realPart*this.realPart + this.imagPart* this.imagPart);
	}

	//getters and setters
	public double getRealPart(){
		return realPart;
	}
	public void setRealPart(double realPart){
		this.realPart = realPart;
	}
	public double getImagPart(){
		return imagPart;
	}
	public void setImagPart(double imagPart){
		this.imagPart = imagPart;
	}
	public static void main(String [] args){
		ComplexNumber cn1 = new ComplexNumber(2, 3.5);
		System.out.printf("real: " + cn1.getRealPart() + "\nimag: " + cn1.getImagPart() + "i");
	}

	public String toString(){
		String val = "" + this.realPart + " + " + this.imagPart; 
		return val;
	}
	public static ComplexNumber add(ComplexNumber firstCN, ComplexNumber secondCN){
		double realPart = firstCN.realPart + secondCN.realPart;
		double imagPart = firstCN.imagPart + secondCN.imagPart;
		return null;
	}
	public static ComplexNumber sub(ComplexNumber firstCN, ComplexNumber secondCN){
		double realPart = firstCN.realPart - secondCN.realPart;
		double imagPart = firstCN.imagPart - secondCN.imagPart;
		return null;
	}
	public static ComplexNumber mult(ComplexNumber firstCN, ComplexNumber secondCN){
		ComplexNumber cn3 = new ComplexNumber();
		double a = firstCN.realPart + secondCN.realPart - firstCN.imagPart * secondCN.imagPart;
		cn3.imagPart = firstCN.imagPart + a - firstCN.realPart * a;
		cn3.realPart = a;
		return cn3;
	}
	public static ComplexNumber div(ComplexNumber firstCN, ComplexNumber secondCN){
		double divisor = secondCN.realPart * secondCN.realPart + secondCN.imagPart * secondCN.imagPart;
		if(divisor == 0.0){
			throw new ComplexNumberDivisionByZero();
		}
		double newrealPart = (firstCN.realPart * secondCN.realPart - firstCN.imagPart * secondCN.imagPart) / divisor;
		firstCN.imagPart = (firstCN.imagPart*secondCN.realPart - firstCN.realPart*secondCN.imagPart) / divisor;
		firstCN.realPart = newrealPart;
		return null;
	}
}