import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class StateWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class StateWorld extends World
{
    private final String START = "START";
    private final String PLAYING = "PLAYING";
    private final String WIN = "WIN";
    private final String LOSE = "LOSE";
    
    private String curState; 

    /**
     * Constructor for objects of class StateWorld.
     * 
     */
    public StateWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        
        // set starting state
        curState = START;
        
        // set background to splash screen
        this.setBackground("splash.png");
    }
    
    public void act() {
        
        if (curState.equals(START) && Greenfoot.isKeyDown("space"))
        {
            curState = PLAYING;
            this.setBackground("black_back.png");
        }
        
        if (curState.equals(PLAYING) && (Greenfoot.isKeyDown("W") || Greenfoot.isKeyDown("w")))
        {
            curState = WIN;
            this.setBackground("green_back.png");
        }
    }
}
