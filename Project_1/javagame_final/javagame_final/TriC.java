import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Class for Herbivore, stampedes human, killed by predator.
 * 
 * @author (Arif Hasnat) 
 * @version (a version number or a date)
 */
public class TriC extends Actor
{
    private final int DIST_TO_MOVE = 10;
    private final int DEG_TO_TURN = 15;
    private final int HUNDRED_PERCENT = 100;
    private final int TURN_PERCENTAGE = 15;
    private final int SMALL_TURN_UPPER_LIMIT = 25;
    /**
     * Method to move the a fish around the world.
     */
    public void act() 
    {
        // check if at edge and turn away if necessary
        if (this.isAtEdge()){
            this.turn(DEG_TO_TURN);
        }
        
        // 15 percent of time, turn a small random amount
        if (Greenfoot.getRandomNumber(HUNDRED_PERCENT) < TURN_PERCENTAGE)
        {
            int smallTurn = Greenfoot.getRandomNumber(SMALL_TURN_UPPER_LIMIT);
            this.turn(smallTurn);
        }
        
        // move
        this.move(DIST_TO_MOVE);
        
        // check to see if we encountered the seahorse
        if (this.getOneIntersectingObject(Player.class) != null) {
            // 1. get a reference to the seahorse object
            Player myMeal = (Player)this.getOneIntersectingObject(Player.class);
            // 2. eat myMeal
            eat(myMeal);
        }    
    } 
    
    /**
     * Kills human.
     */
    private void eat(Player meal) {
        World myWrld = (MyWorld)this.getWorld();
        myWrld.removeObject(meal);
    }
}
