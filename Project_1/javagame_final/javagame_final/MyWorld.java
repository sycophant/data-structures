import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;  

/**
 * Worldspace. It exists.
 * 
 * @author (Arif Hasnat) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{   
    private final int PLAYING = 1;
    private final int FINISHED = 0;
    private int gameState;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        
        // initial state is playing
        gameState = 1;
        
        int centerX = this.getWidth()/2;
        int centerY = this.getHeight()/2;

        // Add triceratops herd
        int tricHerdx = 50;
        int tricHerdy = 50;
        this.addObject(new TriC(), tricHerdx, tricHerdy);
        this.addObject(new TriC(), tricHerdx+25, tricHerdy+25);
        this.addObject(new TriC(), tricHerdx+50, tricHerdy+50);
        
        // Add seahorse
        int playerX = 200;
        int playerY = 450;
        this.addObject(new Player(), playerX, playerY);
        
        // Add a yellow fish
        int raptorX = 50;
        int raptorY = 300;
        this.addObject(new Raptor(), raptorX, raptorY);
    }

    public void act() {
        gameState = 2;
        this.setBackground("gamebg.png");
        if (gameState == 2) {
            // Get count of number of each object in myWorld
            List<TriC> TriCList = this.getObjects(TriC.class);
            int numOfTriC = TriCList.size();
            
            List<Raptor> RaptorList = this.getObjects(Raptor.class);
            int numOfRaptors = RaptorList.size();

            List<Player> PlayerList = this.getObjects(Player.class);
            int numOfPlayers = PlayerList.size();
            
            // Make the String of Object Counts
            String countsString = "Triceratops: " + numOfTriC + " - Raptors: " + numOfRaptors + " - Players: " + numOfPlayers;
            
            // Display the string
            this.showText(countsString, 600, 50);
            
            // Set Win/Lose State
            if (numOfTriC !=0 && numOfPlayers == 1 && numOfRaptors == 0){ //defines win state. Player may win and continue to dodge the herd.
                gameState = 4;
                this.setBackground("youwin.png");
            }else if (numOfPlayers == 0 && numOfRaptors != 0) { //defines lose state.
                gameState = 3;
                this.setBackground("youlose.png");
            }
        
        }
    }
}
 