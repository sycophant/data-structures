import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Class for Predator, goes after herbivore, killed by human.
 * 
 * @author (Arif Hasnat) 
 * @version (a version number or a date)
 */
public class Raptor extends Actor
{
    private final int DIST_TO_MOVE = 15;
    private final int TURN_LIMIT = 179;
    private final int HUNDRED_PERCENT = 100;
    private final int TURN_PERCENTAGE = 15;
    private final int SMALL_TURN_UPPER_LIMIT = 10;
   
    /**
     * Act - do whatever the Raptor wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if (this.isAtEdge()) {
            int degToTurn = Greenfoot.getRandomNumber(TURN_LIMIT);
            this.turn(degToTurn);
        }
        
        // 15 percent of time, turn a small random amount
        if (Greenfoot.getRandomNumber(HUNDRED_PERCENT) < TURN_PERCENTAGE)
        {
            int smallTurn = Greenfoot.getRandomNumber(SMALL_TURN_UPPER_LIMIT);
            this.turn(smallTurn);
        }
        this.move(DIST_TO_MOVE);
        
        if (this.getOneIntersectingObject(TriC.class) != null) {
            // 1. get a reference to the Triceratops object
            TriC myMeal = (TriC)this.getOneIntersectingObject(TriC.class);
            
            // 2. eat the Triceratops
            eat(myMeal);
            
        }
    } 
    
    /**
     * Kills triceratops.
     */
    private void eat(TriC meal) {
        World myWrld = (MyWorld)this.getWorld();
        myWrld.removeObject(meal);
    }
}
