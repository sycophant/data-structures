import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Class for User controlled character. Has to kill the raptors.
 * 
 * @author (Arif Hasnat) 
 * @version (a version number or a date)
 */
public class Player extends Actor
{
    private final int DIST_TO_MOVE = 10;
    private final int DEG_TO_TURN = 5;
    
    /**
     * Act - the act method moves the Player as follows:
     *   left cursor key - move the Player backward
     *   right cursor key - move the Player forward
     *   up cursor key - turn the Player counter clockwise DEG_TO_TURN
     *   down cursor key - turn the Player clockwise DEG_TO_TURN
     */
    public void act() 
    {   // Move forward
        if (Greenfoot.isKeyDown("right")) {
            this.move(DIST_TO_MOVE);
        }
        
        // Move backward
        if (Greenfoot.isKeyDown("left")) {
            this.move(-DIST_TO_MOVE);
        }
        
        // Turn counterclockwise
        if (Greenfoot.isKeyDown("up")) {
            this.turn(-DEG_TO_TURN);
        }
        
        // Turn clockwise
        if (Greenfoot.isKeyDown("down")) {
            this.turn(DEG_TO_TURN);
        }
        
        if (this.getOneIntersectingObject(Raptor.class) != null)
        {
            // 1. get a reference to the Raptor object
            Raptor myKill = (Raptor)this.getOneIntersectingObject(Raptor.class);
            // 2. eat myKill
            eat(myKill);
        }
    }    
    
    /**
     * Kills raptor.
     */
    private void eat(Raptor meal) {
        World myWrld = (MyWorld)this.getWorld();
        myWrld.removeObject(meal);
    }
}
