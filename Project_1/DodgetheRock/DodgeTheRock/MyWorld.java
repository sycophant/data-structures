import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{   
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        
        // Add a rock at the center
        int centerX = this.getWidth()/2;
        int centerY = this.getHeight()/2;
        this.addObject(new Rock(), centerX, centerY);
        
        // Add the first killer fish
        int killerFishX = 50;
        int killerFishY = 50;
        this.addObject(new GreenFish(), killerFishX, killerFishY);
        this.addObject(new YellowFish(), killerFishX+500, killerFishY+600);
        
        // Add the second killer fish
        this.addObject(new GreenFish(), killerFishX+25, killerFishY+25);
        this.addObject(new GreenFish(), killerFishX+75, killerFishY+125);
        this.addObject(new GreenFish(), killerFishX+225, killerFishY+325);
        
        // Add seahorse
        int seahorseX = 200;
        int seahorseY = 450;
        this.addObject(new Seahorse(), seahorseX, seahorseY);
    }
}
 