import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class YellowFish here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class YellowFish extends Actor
{
    private final int DIST_TO_MOVE = 15;
    private final int TURN_LIMIT = 179;
   
    /**
     * Act - do whatever the YellowFish wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if (this.isAtEdge()) {
            int degToTurn = Greenfoot.getRandomNumber(TURN_LIMIT);
            this.turn(degToTurn);
        }
        this.move(DIST_TO_MOVE);
        if (this.getOneIntersectingObject(Rock.class) != null) {
            this.turn(TURN_LIMIT+1);
        }
        
        if (this.getOneIntersectingObject(Seahorse.class) != null) {
            Seahorse myMeal = (Seahorse)this.getOneIntersectingObject(Seahorse.class);
            eat(myMeal);
        }
        if (this.getOneIntersectingObject(GreenFish.class) != null) {
            GreenFish myMeal = (GreenFish)this.getOneIntersectingObject(GreenFish.class);
            eat(myMeal);
        }
    }
        private void eat(Actor meal) {
        World myWrld = (MyWorld)this.getWorld();
        myWrld.removeObject(meal);
    }
}
