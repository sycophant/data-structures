import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class YellowFish here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class YellowFish extends Actor
{
    private final int DIST_TO_MOVE = 15;
    private final int TURN_LIMIT = 179;
    private final int HUNDRED_PERCENT = 100;
    private final int TURN_PERCENTAGE = 15;
    private final int SMALL_TURN_UPPER_LIMIT = 10;
   
    /**
     * Act - do whatever the YellowFish wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if (this.isAtEdge()) {
            int degToTurn = Greenfoot.getRandomNumber(TURN_LIMIT);
            this.turn(degToTurn);
        }
        
        // 15 percent of time, turn a small random amount
        if (Greenfoot.getRandomNumber(HUNDRED_PERCENT) < TURN_PERCENTAGE)
        {
            int smallTurn = Greenfoot.getRandomNumber(SMALL_TURN_UPPER_LIMIT);
            this.turn(smallTurn);
        }
        this.move(DIST_TO_MOVE);
        
       // check to see if we encountered the seahorse or a GreenFish
        if (this.getOneIntersectingObject(Seahorse.class) != null)
        {
            // 1. get a reference to the seahorse object
            Seahorse myMeal = (Seahorse)this.getOneIntersectingObject(Seahorse.class);
            // 2. eat myMeal
            eat(myMeal);
        } 
        else if (this.getOneIntersectingObject(GreenFish.class) != null) {
            // 1. get a reference to the greenfish object
            GreenFish fishMeal = (GreenFish)this.getOneIntersectingObject(GreenFish.class);
            
            // 2. eat the greenfish
            eat(fishMeal);
            
        }
    } 
    
    /**
     * This method removes the object from the world.
     */
    private void eat(Actor meal) {
        World myWrld = (MyWorld)this.getWorld();
        myWrld.removeObject(meal);
    }
}
