import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;  

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{   
    private final int PLAYING = 1;
    private final int FINISHED = 0;
    private int gameState;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        
        // initial state is playing
        gameState = PLAYING;
        
        // Add a rock at the center
        int centerX = this.getWidth()/2;
        int centerY = this.getHeight()/2;
        this.addObject(new Rock(), centerX, centerY);
        
        // Add the first killer fish
        int killerFishX = 50;
        int killerFishY = 50;
        this.addObject(new GreenFish(), killerFishX, killerFishY);
        
        // Add the second killer fish
        this.addObject(new GreenFish(), killerFishX+25, killerFishY+25);
        
        // Add the third killer fish
        this.addObject(new GreenFish(), killerFishX+50, killerFishY+50);
        
        // Add seahorse
        int seahorseX = 200;
        int seahorseY = 450;
        this.addObject(new Seahorse(), seahorseX, seahorseY);
        
        // Add a yellow fish
        int yellowFishX = 50;
        int yellowFishY = 300;
        this.addObject(new YellowFish(), yellowFishX, yellowFishY);
    }
    
    public void act() {
        if (gameState == PLAYING) {
            // Get count of number of each object in myWorld
            List<GreenFish> GreenFishList = this.getObjects(GreenFish.class);
            int numOfGreenFish = GreenFishList.size();
            
            List<Seahorse> SeahorseList = this.getObjects(Seahorse.class);
            int numOfSeahorses = SeahorseList.size();
            
            // Make the String of Object Counts
            String countsString = "GreenFish: " + numOfGreenFish  +
                                  " - Seahorses: " + numOfSeahorses;
            
            // Display the string
            this.showText(countsString, 600, 50);
            
            // Quit if no GreenFish or Seahorses
            if (numOfGreenFish == 0 && numOfSeahorses == 0) {
                gameState = FINISHED;
                this.setBackground("FinishedBackground.png");
            }
        
        }
    }
}
 