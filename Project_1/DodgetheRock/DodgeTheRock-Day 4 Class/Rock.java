import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Rock here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Rock extends Actor
{
    private final int ROCK_WIDTH = 100;
    private final int ROCK_HEIGHT = 100;
    /**
     * Constructor to initialize a rock to the correct size.
     */
    public Rock() {
        GreenfootImage image = this.getImage();
        image.scale(ROCK_WIDTH, ROCK_HEIGHT);
    }
    /**
     * Act - do whatever the Rock wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
