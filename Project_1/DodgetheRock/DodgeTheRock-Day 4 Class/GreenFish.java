import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class GreenFish here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GreenFish extends Actor
{
    private final int DIST_TO_MOVE = 10;
    private final int DEG_TO_TURN = 15;
    private final int HUNDRED_PERCENT = 100;
    private final int TURN_PERCENTAGE = 15;
    private final int SMALL_TURN_UPPER_LIMIT = 25;
    /**
     * Method to move the a fish around the world.
     */
    public void act() 
    {
        // check if at edge and turn away if necessary
        if (this.isAtEdge()){
            this.turn(DEG_TO_TURN);
        }
        
        // 15 percent of time, turn a small random amount
        if (Greenfoot.getRandomNumber(HUNDRED_PERCENT) < TURN_PERCENTAGE)
        {
            int smallTurn = Greenfoot.getRandomNumber(SMALL_TURN_UPPER_LIMIT);
            this.turn(smallTurn);
        }
        
        // move
        this.move(DIST_TO_MOVE);
        
        // check to see if we encountered the seahorse
        if (this.getOneIntersectingObject(Seahorse.class) != null) {
            // 1. get a reference to the seahorse object
            Seahorse myMeal = (Seahorse)this.getOneIntersectingObject(Seahorse.class);
            // 2. eat myMeal
            eat(myMeal);
        }    
    } 
    
    /**
     * This method removes the object from the world.
     */
    private void eat(Seahorse meal) {
        World myWrld = (MyWorld)this.getWorld();
        myWrld.removeObject(meal);
    }
}
