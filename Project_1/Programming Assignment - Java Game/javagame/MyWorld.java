import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Game space for turn-based Java game. Contains 2 actors - player and enemy.
 * Game ends when one actor dies.
 * Actors take loop-based turns in a turn-based game.
 * @author (A. Hasnat) 
 * @version (v. 10-2-19)
 */
public class MyWorld extends World
{
    /**
     * Values being set for the game space.
     */
    public static int gameState = 1;            //playing = 1, player turn = 2, enemy turn = 3, win = 4, lose = 5
    public Player player = new Player();
    public Raptor raptor = new Raptor();
    public TriC tric = new TriC();
    public HUD hudbg = new HUD();
    public Actor enemy;         //assigining enemy.
    public static int playerHP;         //assigning heath pool for player
    public static int enemyHP;          //assigning health pool for enemy.

    /**
     * Constructor. Sets up HUD, spawns objects 
     */
    public MyWorld(){       // 600x400 cell World, 1px cellsize

        super(600, 400, 1);
        this.setBackground("gamebg.png");
        prepare();
        hudbg.act();
    }
    /**
     * Act method. Set for player to always initiate, and to check health pools via method call.
     */
    public void act(){
        gameState = 2;
        stateCheck();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare(){
        int centerX = this.getWidth()/2;
        int centerY = this.getHeight()/2;
        this.addObject(hudbg, centerX, centerY+150);            //most coordinates in classes are eyeballed.
        this.addObject(player, centerX-150, centerY);           // Init player
        playerHP = player.GetHealth();
        if (Greenfoot.getRandomNumber(99)<51){          // init enemy in world, by random number generation.
            this.addObject(raptor, centerX+150, centerY);
            enemy = raptor;
            enemyHP = raptor.GetHealth();
        }else {
            this.addObject(tric, centerX+150, centerY);
            enemy = tric;
            enemyHP = tric.GetHealth();
        }
    }

    /**
     * Death state of an object, triggered by HP = 0.
     * @param Actor Kill method for whatever actor.
     */
    public void ActorDeath(Actor someobj) {
        removeObject(someobj);
    }

    /**
     * Sets game state for endgame conditions.
     */
    private void stateCheck(){
        if (enemyHP <= 0 && playerHP > 0) {            // Win/Lose States
            ActorDeath(enemy);
            gameState = 4;
            this.setBackground("youwin.png");
            }else if (playerHP <= 0){            // Win/Lose States
                ActorDeath(player);
                gameState = 5;
                this.setBackground("youlose.png");
            }
    }
}
