import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Enemy class Raptor and functionality.
 * 
 * @author (A. Hasnat) 
 * @version (v. 10-2-19)
 */
public class Raptor extends Actor
{
    private final int RAPTOR_HEIGHT = 150;
    private final int RAPTOR_WIDTH = 150;
    private static int RAPTOR_HP = 25;
    private String turnNote = "Raptor's turn!'";
    private String atkNote = "Raptor is attacking!";

    /**
     * Constructor, sets rest state.
     */
    public Raptor(){
        setRest();
    }

    /**
     * Sets up behavior of Raptor in game space via method call.
     */
    public void act() {
        MyWorld w = (MyWorld)this.getWorld();
        while (w.gameState==3){
            w.showText(turnNote, 300, 15);
            w.showText(atkNote, 300, 375);
            Player p = w.player;
            if (w.playerHP != 0){
                setAtk();
                w.showText(atkNote, 300, 375);
                RaptorAttack(p);
            }
            Greenfoot.delay(100);
            setRest();
            w.gameState = 2; //turn increment back to player
        }
    }

    /**
     * Getter for health pool.
     */
    public static int GetHealth() {
        return RAPTOR_HP;
    }

    /**
     * Attack method.
     * @param Actor Slashes health pool of whichever actor is targeted.
     */
    public void RaptorAttack(Player player){
        MyWorld.playerHP = MyWorld.playerHP - (2 * Greenfoot.getRandomNumber(5));           //reduce player HP with a damage range
    }

    /**
     * Set rest state audiovisual elements.
     */
    public void setRest(){
        GreenfootImage rest = new GreenfootImage("raptorrest.png");
        rest.scale(RAPTOR_HEIGHT, RAPTOR_WIDTH);
        this.setImage(rest);
    }

    /**
     * Set attack state audiovisual elements.
     */
    public void setAtk(){
        GreenfootImage atk = new GreenfootImage("raptoratk.png");
        atk.scale(RAPTOR_HEIGHT, RAPTOR_WIDTH);
        this.setImage(atk);
    }
}
