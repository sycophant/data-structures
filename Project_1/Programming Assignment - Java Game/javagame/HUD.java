import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Sets up HUD for the game space.
 * 
 * @author (A. Hasnat) 
 * @version (v. 10-2-19)
 */
public class HUD extends Actor
{
	private String gameStart = "GAME START";
    /**
     * Act method for HUD. Minor bug for endgame states.
     */
    public void act() {
        MyWorld w = (MyWorld)this.getWorld();
        int playerHP = w.playerHP;
    	int enemyHP = w.enemyHP;
    	w.showText(gameStart, 300, 350);
    	if (w.gameState >= 2 || playerHP <= 0 || enemyHP <= 0){			//state check
    		String gameStart = "";
    		w.showText(gameStart, 300, 350);
        	String health = "Player\n   HP:" + playerHP + "\nEnemy\n   HP:" + enemyHP;      // HUD in world.
        	w.showText(health, 75, 350); 
    	}
    }    
}
