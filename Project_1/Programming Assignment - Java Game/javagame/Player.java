import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Scanner;

/**
 * Player class and functionality.
 * 
 * @author (A. Hasnat) 
 * @version (v. 10-2-19)
 */
public class Player extends Actor{

    private static int PLAYER_HP = 40;
    private final int PLAYER_HEIGHT = 150;
    private final int PLAYER_WIDTH = 150;
    private String turnNote = "Player's turn!'";
    private String atkNote = "Player is attacking!";

    /**
     * Constructor, sets rest state.
     */
    public Player(){
        setRest();
    }

    /**
     * Sets up behavior of Player in game space via method call.
     */
    public void act(){
      MyWorld w = (MyWorld)this.getWorld();
      while(w.gameState == 2){
        w.showText(turnNote, 300, 15);
        Scanner reader = new Scanner(System.in);      //wait for player input.
        System.out.println("Type 1 to Attack");
        int inputValue = reader.nextInt();
        reader.close();
        if (inputValue == 1){
          setAtk();
          w.showText(atkNote, 275, 330);
          Actor e = w.enemy;      //placeholder for enemy defined in MyWorld.
          PlayerAttack(e);
          }else if (inputValue != 1) {
            System.out.println("Placeholder.");     //placeholder, implement switch and corresponding method for more functionality
          }
          Greenfoot.delay(100);
          w.gameState = 3;
        }
        setRest();
      }

    /**
     * Getter for player health pool.
     */
    public static int GetHealth() {
        return PLAYER_HP;
    }

    /**
     * Attack method for player.
     * @param Actor Slashes health pool of whichever actor is targeted.
     */
    public void PlayerAttack(Actor enemy){
        MyWorld.enemyHP = MyWorld.enemyHP - Greenfoot.getRandomNumber(15); //reduce enemy HP with a damage range
    }

    /**
     * Set attack state audiovisual elements.
     */
    public void setAtk(){
      GreenfootImage atk = new GreenfootImage("playeratk.png");
      atk.scale(PLAYER_HEIGHT, PLAYER_WIDTH);
      this.setImage(atk);
    }

    /**
     * Set rest state audiovisual elements.
     */
    public void setRest(){
      GreenfootImage rest = new GreenfootImage("playerrest.png");
      rest.scale(PLAYER_HEIGHT, PLAYER_WIDTH);
      this.setImage(rest);
    }
}