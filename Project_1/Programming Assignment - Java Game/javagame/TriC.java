import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class TriC here.
 * 
 * @author (A. Hasnat) 
 * @version (v. 10-2-19)
 */
public class TriC extends Actor
{
    private final int TRIC_HEIGHT = 150;
    private final int TRIC_WIDTH = 150;
    private static int TRIC_HP = 50;
    public int THREAT_LEVEL = 0;
    private String turnNote = "Triceratops' turn!'";
    private String atkNote = "Triceratops is attacking!";
    private String chgNote = "Triceratops is getting angry...";

    /**
     * Constructor, sets rest state.
     */
    public TriC() {
        setRest();
    }

    /**
     * Sets up behavior of Triceratops in game space via method call.
     */
    public void act() {
        MyWorld w = (MyWorld)this.getWorld();
        while (w.gameState==3){
            w.showText(turnNote, 300, 15);
            THREAT_LEVEL += 5;          //Unique behavior, Triceratops only attacks if threat level is above a certain threshold.
            Player p = w.player;
            if (w.playerHP>0 && THREAT_LEVEL>25){
                setAtk();
                w.showText(atkNote, 300, 375);
                TriCAttack(p);
            }else {w.showText(chgNote, 300, 375);}
            Greenfoot.delay(100);
            setRest();
            w.gameState = 2; //turn increment
        }
    }

    /**
     * Getter for health pool.
     */
    public static int GetHealth() {
        return TRIC_HP;
    }

    /**
     * Attack method.
     * @param Actor Slashes health pool of whichever actor is targeted.
     */
    public void TriCAttack(Player player){
        MyWorld.playerHP = MyWorld.playerHP - 35; //reduce player HP.
        THREAT_LEVEL = 0;
    }

    /**
     * Set rest state audiovisual elements.
     */
    public void setRest(){
        GreenfootImage rest = new GreenfootImage("tricrest.png");
        rest.scale(TRIC_HEIGHT, TRIC_WIDTH);
        this.setImage(rest);
    }

    /**
     * Set attack state audiovisual elements.
     */
    public void setAtk(){
        GreenfootImage atk = new GreenfootImage("tricatk.png");
        atk.scale(TRIC_HEIGHT+100, TRIC_WIDTH);
        this.setImage(atk);
    }
}
