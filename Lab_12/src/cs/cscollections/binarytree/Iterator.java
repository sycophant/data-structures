package cs.cscollections.binarytree;

public interface Iterator<T>{
	boolean hasNext();
	T next();
	void remove();
	void inOrderTrav();
}