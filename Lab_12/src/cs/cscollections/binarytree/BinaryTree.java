package cs.cscollections.binarytree;
import java.util.Iterator;

public class BinaryTree<T> implements BinaryTreeADT<T>{
	protected T root;
	protected int size;
	protected BinaryTree<T> left;
	protected BinaryTree<T> right;

	BinaryTree(T ele, BinaryTree<T> left, BinaryTree<T> right){
		this.root = ele;
		this.left = left;
		this.right = right;
		if (size!=0) this.size += left.size() + 1 + right.size();
	}
	BinaryTree(T ele){
		this.left = null;
		this.right = null;
		this.root = ele;
		this.size = 1;
	}
	BinaryTree(){
		this.left = null;
		this.right = null;
		this.root = null;
		this.size = 0;
	}

	public T root(){return this.root;}
	public boolean isEmpty(){return size ==0;}
	public int size(){return this.size;}
	public BinaryTreeADT<T>leftSubtree(){return this.left;}
	public BinaryTreeADT<T>rightSubtree(){return this.right;}
	public Iterator iteratorInOrder(){return new TreeIterator(this, this.size);}
}