package cs.cscollections.binarytree;
import java.util.Iterator;

public interface BinaryTreeADT<T>{
	T root();
	BinaryTreeADT<T> leftSubtree();
	BinaryTreeADT<T> rightSubtree();
	boolean isEmpty();
	int size();
	Iterator<T> iteratorInOrder();
}