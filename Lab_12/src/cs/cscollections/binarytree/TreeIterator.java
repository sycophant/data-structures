package cs.cscollections.binarytree;
import java.util.Iterator;

public class TreeIterator<T> implements Iterator<T>{
	private int current;
	private int count;
	private T[] collectionArray;
	private BinaryTree<T> collection;

	TreeIterator(BinaryTree<T> collection, int size){
		current = 0;
		count = size;
		collectionArray = (T []) new Object[count];
		inOrderTrav(collection);
		current = 0;
		this.collection = collection;
	}

	public boolean hasNext(){return current < count;}
	public T next(){
		T retVal = collectionArray[current];
		current++;
		return retVal;
	}
	public void remove(){}
	public void inOrderTrav(BinaryTree<T> tree){
		if(tree==null) return;
			else{
			inOrderTrav(tree.left); 
        	collectionArray[current] = tree.root; 
        	inOrderTrav(tree.right); 
			}
	}
}