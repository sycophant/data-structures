package cs.cscollections.sorts;

import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;

//quicksort implementation.
public class Sorts{
    private static int compCount;
    public static int []a;

    public static void setArray(int[] ar) {
        a = ar;
    }


    public static void sort(){  //sets indexes, calls quicksort
        int left = 0;
        int right = a.length - 1;
        quickSort(left, right);
    }
    public static void quickSort(int left, int right){
        if(left>=right)return;  //ends quicksort if sentinel is hit
        int pivot = a[right];   //calls pivot from array
        int partition = partition(left, right, pivot);
        quickSort(0, partition-1);  //called on left part
        quickSort(partition+1, right);  //called on right part
    }
    public static int partition(int left, int right, int pivot){
        int headPos = left-1;
        int tailPos = right;
        while(headPos < tailPos){
            while(a[++headPos]<pivot){setComp();}   //first comparison
            while(tailPos>0 && a[--tailPos]>pivot){setComp();}  //second comparison
            if(headPos>=tailPos){   //third comparison
                setComp();
                break;
            }else swap(headPos, tailPos);
            setComp();
        }swap(headPos, right);
        return headPos;
    }
    public static void swap(int left, int right){   //swap elements from array
        int temp = a[left];
        a[left] = a[right];
        a[right] = temp;
    }
    
    public static void clearComp(){compCount = 0;}  //clears count
    public static int getComp(){return compCount;}  //pull the count
    public static void setComp(){compCount++;}  //increment count
    public static void printFile(){for(int i:a){System.out.printf(i+" ");}} //print the file
}
