package cs.csmath.exceptions;

public class ComplexNumberDivisionByZero extends RuntimeException{
	public ComplexNumberDivisionByZero(){
		super("Attempt to divide by zero");
	}
}