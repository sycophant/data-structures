package RoundRobin;
import RoundRobin.RoundRobinEngine;
/**
 * Wrapper class for round robin execution, calls methods from RoundRobinEngine.
 * Prints out an initial log, thread executions times and an end log.
 * @author A. Hasnat
 * @version v1.0
 */
public class RoundRobinApp{
    private int thrCount;
    private int timeCount;
    private final int TIME_SLICE = 20;
    /**
     * Main method. Instantiates the class and calls the run method.
     * @param args
     */
    public static void main(String[] args){//main function instantiates object wraps for method
        RoundRobinApp threadRunner = new RoundRobinApp();
        threadRunner.run();
    }
    /**
     * Instantiates RoundRobinEngine and calls the relevant methods.
     */
    void run(){//main method calls from engine class
        RoundRobinEngine robin = new RoundRobinEngine(TIME_SLICE);
        thrCount = robin.getThreadCount();
        printDescription(thrCount);
        robin.printStartLog();
        robin.printLog();
        System.out.println("Execution time: "+robin.getTimeCount()+" ms");
    }
    /**
     * Initial setup, prints messages for readability.
     * @param thrCount
     */
    void printDescription(int thrCount){//print methods
        System.out.println("Thread count"+thrCount);
        System.out.println("Exec time in ms");
        System.out.println("Max: "+" Avg: "+" Min: ");
        System.out.println("\nExecution Log");
    }
}