package RoundRobin;
/**
 * Method that sets up a task with time measurement and thread ID built in.
 * @author A. Hasnat
 * @version v1.0
 */
public class CSThread{
    private int execTime;
    private String threadID;
    /**
     * constructor, sets up id and time count from declared variables.
     * @param execTime
     * @param threadID
     */
    CSThread(int execTime, String threadID){
        this.execTime = execTime;
        this.threadID = threadID;
    }
    /**
     * Getters.
     */
    int getExecTime(){return this.execTime;}
    String getThreadId(){return this.threadID;}
    /**
     * Simulates running a thread, has time and ID built in.
     * @param execTime
     * @param threadID
     */
    void doWork(int timeSlice){
        if(this.execTime-timeSlice > 0){
            this.execTime = this.execTime-timeSlice;
            System.out.print("Thread ID: "+threadID+" exec time: "+execTime+" ms\n");
        }
        else{this.execTime = execTime-timeSlice;
            System.out.println("Clear!\n");
        }
    }
}
