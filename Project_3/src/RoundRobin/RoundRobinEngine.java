package RoundRobin;
import java.util.ArrayDeque;

/**
 * RoundRobinEngine has methods for implementing round robin scheduling.
 * Set up via arraydeque to iterate through a queue.
 * @author A. Hasnat
 * @version v1.0
 */
    public class RoundRobinEngine{
    private int thrCount;
    private ArrayDeque<CSThread> thrB;
    private CSThread[] thrA;
    private int timeCount;
    private int timeSlice;
    /**
     * Constructor, sets up instantiated objects as threads according to specifications
     * @param timeSlice
     */
    RoundRobinEngine(int timeSlice){
        thrCount = ((int)(Math.random() * ((20 - 10) + 1))+ 10);
        this.timeSlice = timeSlice;
        timeCount = 0;
        thrA = new CSThread[thrCount];
        thrB = new ArrayDeque<CSThread>();
        for(int i = 0; i < thrCount; i++){
            CSThread thread = new CSThread(((int)(Math.random()*((5000-10)+1))+10),"Thr"+i);
            thrB.addLast(thread);
            thrA[i] = thread;
        }
    }
    /**
     * prints initial log for number of threads called.
     */
    void printStartLog(){for(int i=0;i<thrCount;i++){System.out.println("Thread: "+thrA[i].getThreadId()+" Time: "+thrA[i].getExecTime()+" ms");}}
    /**
     * prints thread status, arraydeque is called here to iterate through threads.
     */
    void printLog(){//nested loops setting up queue for round robin
        while(!thrB.isEmpty()){
            for(int i = 0; i < thrCount; i++){
                CSThread thread = thrB.peekFirst();
                thread.doWork(timeSlice);//execs thread within allotted time
                if(thread.getExecTime()>0){//swaps threds by exec time
                    thrB.addLast(thrB.peekFirst());
                    thrB.removeFirst();
                    addTime(timeSlice);
                }else{
                    thrB.removeFirst();
                    --thrCount;}
            }System.out.println();
        }System.out.println();
    }
    /**
     * increments total timecount by timeslice of each individual thread.
     */
    void addTime(int timeSlice){this.timeCount+=timeSlice;}
    /**
     * Getters
     */
    int getThreadCount(){return this.thrCount;}
    int getTimeCount(){return this.timeCount;}
    
}