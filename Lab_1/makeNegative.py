def makeNegative(pict) :
  # makeNegative convert a picture to its negative. Note
  # that the original picture is altered by this method.
  #
  # pict - the picture to convert
  
  for pixel in getPixels(pict) :
    redVal = getRed(pixel)
    greenVal = getGreen(pixel)
    blueVal = getBlue(pixel)
    setRed(pixel, 255 - redVal)
    setGreen(pixel, 255 - greenVal)
    setBlue(pixel, 255 - blueVal)
