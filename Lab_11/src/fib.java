import java.util.Scanner;

class fib{
	private static Scanner uin = new Scanner(System.in);
	private static long n;
	private static long counter;
	public static void main(String[] args){
		getInput();
		System.out.println(printFib(fibI(n)) + " Being iterative\n");
		System.out.println(counter + " Being the number of iterations\n");
		clearCount();
		System.out.println(printFib(fibR(n)) + " Being recursive\n");
		System.out.println(counter + " Being the number of iterations\n");
		clearCount();
	}
	static long fibI(long n){
		if(n<=1){return n;}
		counter++;
		int fibCur = 1;
		int fibTrail = 1;
		for(int i=2; i<n; i++){
			int temp = fibCur;
			fibCur+=fibTrail;
			fibTrail = temp;
			counter++;
		}
		return fibCur;
	}
	static long fibR(long n){
		if(n<=1){return n;}
		counter++;
		return fibR(n-1)+fibR(n-2);
	}
	static void getInput(){
		System.out.printf("Please input an integer for a fibonacci sequence: ");
		n = uin.nextInt();
	}
	static long printFib(long a){return a;}
	static void clearCount(){counter=0;}
}