package cs.cscollections;

public class SinglyLinkedList<E> implements List<E>{
	private Node<E> head;
	private int size = 0;

	private Node<E> findNode(E target){
		Node<E> curNode = head;
		while (curNode != null && ((Comparable<E>)(curNode.getData())).compareTo(target) != 0){
			curNode = curNode.getNext();
		}
		return curNode;
	}
	private void insertAfter(Node<E> curNode, Node<E> newNode){
		if(this.head == null){
			this.head = newNode;
			size++;
		}else{
			newNode.setNext(curNode.getNext());
			curNode.setNext(newNode);
			size++;
		}
	}

	public SinglyLinkedList(){}
	public void append(E ele){
		if (head == null){
			prepend(ele);
		}else{
			Node<E> curNode = head;
			while(curNode.getNext() != null){
				curNode = curNode.getNext();
				size++;
			}
			curNode = new Node<E>(ele);
		}
	}
	public void prepend(E ele){
		Node<E> newNode = new Node<E>(ele);
		if(this.head != null){
			newNode.setNext(this.head);
		}
		this.head = newNode;
		size++;
	}
	public void insertAfter(E first, E second){
		if (findNode(first) == null){
			insertAfter(first, second);
		}
	}
	public void remove(E ele){}
	public boolean search(E ele){
		return !(findNode(ele) == null);
	}
	public boolean isEmpty(){
			return head == null;
		}
	public int getLength(){
		return size;
	}
	public void printList(){
		Node<E> curNode = head;
		System.out.println("List: ");
		while (curNode != null){
			System.out.println("  " + curNode.getData());
			curNode = curNode.getNext();
		}
	}
}