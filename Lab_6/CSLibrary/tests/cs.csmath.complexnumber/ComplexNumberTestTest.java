package cs.csmath.complexnumber;
import org.junit.Rule;
import org.junit.Test;
import static org.junit.Assert.*;

public class ComplexNumberTestTest {

    static final double DELTA = 0.00001;

    @Test
    public void testAdd() {
        ComplexNumber cn1 = new ComplexNumber (5.5, 2.0);
        ComplexNumber cn2 = new ComplexNumber (9.0, 3.0);
        cn1.add(cn2);
        assertEquals( 9.5, cn1.getRealPart(), DELTA);
        assertEquals(7.0,cn1.getImagPart(), DELTA);
    }
    @Test
    public void testSub() {
        ComplexNumber cn1 = new ComplexNumber (4.5, 3.0);
        ComplexNumber cn2 = new ComplexNumber (6.0, 4.0);
        cn1.sub(cn2);
        assertEquals( -1.5, cn1.getRealPart(), DELTA);
        assertEquals(-1.0,cn1.getImagPart(), DELTA);
    }
    @Test
    public void testZeroMult(){
        ComplexNumber cn1 = new ComplexNumber(2.0, 1.6);
        ComplexNumber cn2 = new ComplexNumber(0.0,0.0);
        cn1.mult(cn2);
        assertEquals(0.0, cn1.getRealPart(),DELTA);
        assertEquals(0.0,cn1.getImagPart(), DELTA);
    }

    @Test
    public void testPosMultPos(){
        ComplexNumber cn3 = new ComplexNumber(2.0, 1.5);
        ComplexNumber cn4 = new ComplexNumber(1.0,3.0);
        cn3.mult(cn4);
        assertEquals(-2.5, cn3.getRealPart(),DELTA);
        assertEquals(7.5,cn3.getImagPart(), DELTA);
    }

    @Test
    public void testNegMultNeg(){
        ComplexNumber cn5 = new ComplexNumber(-2.0, -1.5);
        ComplexNumber cn6 = new ComplexNumber(-1.0,-3.0);
        cn5.mult(cn6);
        assertEquals(-2.5, cn5.getRealPart(),DELTA);
        assertEquals(7.5,cn5.getImagPart(), DELTA);
    }

    @Test
    public void testPosMultNeg(){
        ComplexNumber cn7 = new ComplexNumber(3.0, 2.0);
        ComplexNumber cn8 = new ComplexNumber(-1.0,-3.0);
        cn7.mult(cn8);
        assertEquals(3.0, cn7.getRealPart(),DELTA);
        assertEquals(-11.0,cn7.getImagPart(), DELTA);
    }

    @Test
    public void testPosPosDiv() {
        ComplexNumber cn3 = new ComplexNumber(3.0, 2.0);
        ComplexNumber cn4 = new ComplexNumber(1.0,3.0);
        cn3.div(cn4);
        assertEquals(0.9, cn3.getRealPart(),DELTA);
        assertEquals(-0.7,cn3.getImagPart(), DELTA);
    }

    @Test
    public void testNegPosDiv(){
        ComplexNumber cn5 = new ComplexNumber(-3.0, -2.0);
        ComplexNumber cn6 = new ComplexNumber(1.0,3.0);
        cn5.div(cn6);
        assertEquals(-0.9, cn5.getRealPart(),DELTA);
        assertEquals(0.7,cn5.getImagPart(), DELTA);
    }

    @Test
    public void testPosNegDiv(){
        ComplexNumber cn1 = new ComplexNumber(3.0, 2.0);
        ComplexNumber cn2 = new ComplexNumber(-1.0,-3.0);
        cn1.div(cn2);
        assertEquals(-0.9, cn1.getRealPart(),DELTA);
        assertEquals(0.7,cn1.getImagPart(), DELTA);
    }

    @Test
    public void testZeroDiv(){
        ComplexNumber cn7 = new ComplexNumber(3.0, 2.0);
        ComplexNumber cn8 = new ComplexNumber(0.0,0.0);
        cn8.div(cn8);
    }

    @Test
    public void testConjPosImagPart(){
        ComplexNumber cn1 = new ComplexNumber (3.0,2.0);
        ComplexNumber cn2 = cn1.conj();
        assertEquals(3.0,cn2.getRealPart(),DELTA);
        assertEquals(-2.0,cn2.getImagPart(), DELTA);
    }

    @Test
    public void testConjNegImagPart(){
        ComplexNumber cn3 = new ComplexNumber (3.0,-2.0);
        ComplexNumber cn4 = cn3.conj();
        assertEquals(3.0,cn4.getRealPart(),DELTA);
        assertEquals(2.0,cn4.getImagPart(), DELTA);
    }

    @Test
    public void testConjZeroImagPart(){
        ComplexNumber cn3 = new ComplexNumber (3.0,0.0);
        ComplexNumber cn4 = cn3.conj();
        assertEquals(3.0,cn4.getRealPart(),DELTA);
        assertEquals(0.0,cn4.getImagPart(), DELTA);
    }

    @Test
    public void testPosPosAbs(){
        ComplexNumber cn = new ComplexNumber (3.0,4.0);
        assertEquals(5.0,cn.abs(),DELTA);
    }

    @Test
    public void testPosNegAbs(){
        ComplexNumber cn1 = new ComplexNumber (3.0,-4.0);
        assertEquals(5.0,cn1.abs(),DELTA);
    }

    @Test
    public void testNegNegAbs(){
        ComplexNumber cn2 = new ComplexNumber (-3.0,-4.0);
        assertEquals(5.0,cn2.abs(),DELTA);
    }

    @Test
    public void testNegPosAbs(){
        ComplexNumber cn3 = new ComplexNumber (-3.0,4.0);
        assertEquals(5.0,cn3.abs(),DELTA);
    }

    @Test
    public void testPosZeroAbs(){
        ComplexNumber cn3 = new ComplexNumber (0.0,4.0);
        assertEquals(4.0,cn3.abs(),DELTA);
    }

    @Test
    public void testZeroAbs(){
        ComplexNumber cn3 = new ComplexNumber (0.0,0.0);
        assertEquals(0.0,cn3.abs(),DELTA);
    }

    public void getImagPart() {
        ComplexNumber cn = new ComplexNumber(3.2, -3.3);
        double imagPart = cn.getImagPart();
        assertEquals( -3.3, imagPart, DELTA);
    }

    @Test
    public void setRealPart() {
        ComplexNumber cn = new ComplexNumber(5.2, 2.6);
        cn.setRealPart(2.6);
        assertEquals(2.6, cn.getRealPart(), DELTA);
    }

    @Test
    public void setImagPart() {
        ComplexNumber cn = new ComplexNumber(5.2, 2.6);
        cn.setImagPart(5.2);
        assertEquals( 5.2, cn.getImagPart(), DELTA);
    }
}